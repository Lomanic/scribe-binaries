# Scribe binary releases

Binaries built on CI so you don't have to install the whole crystal/lucky toolchain.

```bash
curl -L "https://gitlab.com/Lomanic/scribe-binaries/-/jobs/artifacts/master/download?job=linux_amd64" -o scribe.zip # for linux x64
unzip scribe.zip
cd scribe
# create a config/watch.yml like echo -e "port: 8088\nhost: 0.0.0.0\ndatabase: postgres://does@not/matter" > ./config/watch.yml
./scribe
```

A prebuilt image is also available for linux/amd64 and linux/arm64 with `docker run -it --rm -p 8088:8088 -e SCRIBE_PORT=8088 -e SCRIBE_HOST=0.0.0.0 -e SCRIBE_DB=postgres://does@not/matter registry.gitlab.com/lomanic/scribe-binaries:latest`
